  file {'sbtJar':
    path => '/home/{$id}/bin/sbt-launch.jar',
    source => '/home/{$id}/config/files/sbt-launch.jar',
    mode => 0777,
    ensure => present;
} 
  file {'sbtExecutable':
    path => '/home/$id/bin/sbt',
    content => 'java -Xms1g -Xmx2g -Xss1M -XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=384M -jar `dirname $0`/sbt-launch.jar "$@"',
    mode => 0777,
    ensure => present;
} 
