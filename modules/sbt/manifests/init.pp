class sbt {
  file {'sbtExecutable':
    path => '/home/${user}/bin/sbt',
    content => 'java -Xms512M -Xmx1536M -Xss1M -XX:+CMSClassUnloadingEnabled -XX:MaxPermSize=384M -jar `dirname $0`/sbt-launch.jar "$@"',
    mode => 0777,
    ensure => present;
} 
}
